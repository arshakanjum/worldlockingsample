using UnityEngine;
using Microsoft.MixedReality.WorldLocking.Core;
using Microsoft.MixedReality.WorldLocking.Tools;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.SpatialAwareness;

public class Dashboard : MonoBehaviour
{

    /// <summary>
    /// Use current instance of WorldLockingManager.
    /// </summary>
    private WorldLockingManager worldLockingManager { get { return WorldLockingManager.GetInstance(); } }
    
    /// <summary>
    /// The text manager keeps text display fields up to date, including showing and hiding them as desired.
    /// This may be null if no debug display is required.
    /// </summary>
    [SerializeField]
    [Tooltip("Text manager controlling display of diagnostics.")]
    private StatusToText textManager = null;

    /// <summary>
    /// The anchor visualizer provides a visualization of all existing anchors, including maintenance
    /// such as hiding and showing the anchors.
    /// </summary>
    [SerializeField]
    [Tooltip("Manager controlling display of anchor visualizations.")]
    private AnchorGraphVisual anchorVisualizer = null;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ToggleWireframe()
    {
        // Get the first Mesh Observer available, generally we have only one registered
        var observer = CoreServices.GetSpatialAwarenessSystemDataProvider<IMixedRealitySpatialAwarenessMeshObserver>();
        if (observer.DisplayOption == SpatialAwarenessMeshDisplayOptions.None)
        {
            observer.DisplayOption = SpatialAwarenessMeshDisplayOptions.Occlusion;
        }
        else
        {
            observer.DisplayOption = SpatialAwarenessMeshDisplayOptions.None;
        }
    }

    public void ToggleMetrics(){
        InfoEnabled = !InfoEnabled;
        MetricsEnabled = !MetricsEnabled;
        StatusEnabled = !StateEnabled;
        StateEnabled = !StateEnabled;
    }

    public void ToggleWorldLocking(){
        ManagerEnabled = !ManagerEnabled;
    }


    /// <summary>
    /// Whether the WorldLockingManager is actively stabilizing space or being bypassed.
    /// </summary>
    public bool ManagerEnabled
    {
        get
        {
            return worldLockingManager.Enabled;
        }
        set
        {
            var config = worldLockingManager.Settings;
            config.Enabled = value;
            worldLockingManager.Settings = config;
        }
    }

    /// <summary>
    /// Toggle info display.
    /// </summary>
    public bool InfoEnabled
    {
        get
        {
            if (textManager != null)
            {
                return textManager.InfoEnabled;
            }
            return false;
        }
        set
        {
            if (textManager != null)
            {
                textManager.InfoEnabled = value;
            }
        }
    }

    /// <summary>
    /// Toggle Metrics display
    /// </summary>
    public bool MetricsEnabled
    {
        get
        {
            if (textManager != null)
            {
                return textManager.MetricsEnabled;
            }
            return false;
        }
        set
        {
            if (textManager != null)
            {
                textManager.MetricsEnabled = value;
            }
        }
    }

    /// <summary>
    /// Toggle status display
    /// </summary>
    public bool StatusEnabled
    {
        get
        {
            if (textManager != null)
            {
                return textManager.ErrorStatusEnabled;
            }
            return false;
        }
        set
        {
            if (textManager != null)
            {
                textManager.ErrorStatusEnabled = value;
            }
        }
    }

    /// <summary>
    /// Toggle state display
    /// </summary>
    public bool StateEnabled
    {
        get
        {
            if (textManager != null)
            {
                return textManager.StateIndicatorEnabled;
            }
            return false;
        }
        set
        {
            if (textManager != null)
            {
                textManager.StateIndicatorEnabled = value;
            }
        }
    }
}
